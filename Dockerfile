FROM nginx
WORKDIR /
RUN pwd
RUN ls
COPY app.html /var/www/html/index.html
EXPOSE 8080
CMD ["nginx", "-g", "daemon on;"]